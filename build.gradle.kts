plugins {
	kotlin("multiplatform") version "1.8.0"
	`maven-publish`
}

group = "me.doomsdayrs"
version = "1.0.1"

repositories {
	mavenCentral()
	maven("https://jitpack.io")
}

kotlin {
	explicitApi()
	jvm {
		compilations.all {
			kotlinOptions.jvmTarget = "17"
		}
		withJava()
		testRuns["test"].executionTask.configure {
			useJUnitPlatform()
		}
	}
	/*
	js(BOTH) {
		browser {
			commonWebpackConfig {
				cssSupport.enabled = true
			}
		}
	}


	val hostOs = System.getProperty("os.name")
	val isMingwX64 = hostOs.startsWith("Windows")
	val nativeTarget = when {
		hostOs == "Mac OS X" -> macosX64("native")
		hostOs == "Linux" -> linuxX64("native")
		isMingwX64 -> mingwX64("native")
		else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
	}

	 */

	sourceSets {
		val commonMain by getting {
			dependencies {
				api("com.gitlab.Doomsdayrs:trixnityx:1.0.1")
			}
		}
		val commonTest by getting {
			dependencies {
				implementation(kotlin("test"))
				implementation("io.ktor:ktor-client-cio:2.2.2")
			}
		}
		val jvmMain by getting
		val jvmTest by getting
		//val jsMain by getting
		// val jsTest by getting
		//  val nativeMain by getting
		//   val nativeTest by getting
	}
}
