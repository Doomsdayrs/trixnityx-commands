/*
 * Trixnityx-command : command extensions for trixnity
 * Copyright (C) 2022 Doomsdayrs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package me.doomsdayrs.trixnityx.commands

import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.room.message.MessageBuilder
import net.folivo.trixnity.client.store.Room
import net.folivo.trixnity.client.store.RoomUser
import net.folivo.trixnity.client.store.TimelineEvent

/**
 * Defines action in response to a command
 */
public interface CommandAction {
	/**
	 * Client to work with
	 */
	public val client: MatrixClient

	/**
	 * Language to respond with
	 */
	public val lang: String

	/**
	 * Event that caused this action
	 */
	public val event: TimelineEvent

	/**
	 * Message content that caused the event
	 */
	public val message: String

	/**
	 * Arguments that the message was provided with
	 */
	public val arguments: List<String>

	/**
	 * Attempts to get the user who invoked the command
	 */
	public suspend fun getAuthor(): RoomUser?

	/**
	 * Attempts to get the room where the command was invoked
	 */
	public suspend fun getRoom(): Room?

	/**
	 * Define a message to respond with
	 */
	public suspend fun respond(
		asReply: Boolean = true,
		message: suspend MessageBuilder.() -> Unit
	)
}