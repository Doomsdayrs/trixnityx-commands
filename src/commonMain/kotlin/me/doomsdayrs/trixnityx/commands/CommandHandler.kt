/*
 * Trixnityx-command : command extensions for trixnity
 * Copyright (C) 2022 Doomsdayrs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package me.doomsdayrs.trixnityx.commands

import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.store.TimelineEvent
import net.folivo.trixnity.core.model.events.m.room.RoomMessageEventContent.TextMessageEventContent

public typealias InterceptorFunction = (
	event: TimelineEvent,
	command: CommandData
) -> Boolean

/**
 * Command handler for Trixnity.
 *
 * Simply create an instance of this class, then feed events from
 * getTimelineEventsFromNowOn into [feedEvent].
 */
public class CommandHandler(private val client: MatrixClient) {
	private val commands = arrayListOf<CommandData>()
	private val preCommandListeners = arrayListOf<() -> Unit>()
	private var interceptor: InterceptorFunction = { _, _ -> true }

	/**
	 * Feed an event into the command handler.
	 *
	 * Typically, from `getTimelineEventsFromNowOn`.
	 */
	public suspend fun feedEvent(event: TimelineEvent) {
		if (event.content?.isSuccess == true) {
			val content = event.content?.getOrNull()
			if (content is TextMessageEventContent) {
				val body = content.body
				if (body.startsWith(PREFIX)) {
					val commandString = body.substring(
						body.indexOf(PREFIX) + PREFIX.length,
						body.indexOf(" ").takeIf { it != -1 } ?: body.length
					)

					for (command in commands) {
						val names = command.names.toList()
						val found = names.indexOfFirst { name ->
							name.second.equals(
								commandString,
								ignoreCase = true
							)
						}
						if (found != -1) {
							if (interceptor(event, command)) {
								preCommandListeners.forEach { it() }
								command.action(
									TextMessageEventResponse(
										client = client,
										event = event,
										lang = names[found].first,
										message = body.trim()
									)
								)
							}
							break
						}
					}
				}
			}
		}
	}

	/**
	 * Add an action to occur before a command is executed
	 */
	public fun addOnPreCommandListener(listener: () -> Unit) {
		preCommandListeners.add(listener)
	}

	/**
	 * Add an [interceptor] to occur before a command action occurs.
	 */
	public fun setInterceptor(interceptor: InterceptorFunction) {
		this.interceptor = interceptor
	}

	/**
	 * Get registered commands, incredibly useful for help commands.
	 */
	public fun getCommands(): List<CommandData> =
		commands.toList()

	/**
	 * Register a single command
	 */
	public fun registerCommand(command: CommandData) {
		commands.add(command)
	}

	/**
	 * Register a list of commands
	 */
	public fun registerCommands(commands: List<CommandData>) {
		this.commands.addAll(commands)
	}

	/**
	 * Register commands via command scope, for DSL
	 */
	public fun register(scope: CommandDefineScope.() -> Unit) {
		register(CommandDefineScope().also(scope))
	}

	/**
	 * Register commands via command scope
	 */
	public fun register(scope: CommandDefineScope) {
		registerCommands(scope.commands)
	}
}