/*
 * Trixnityx-command : command extensions for trixnity
 * Copyright (C) 2022 Doomsdayrs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package me.doomsdayrs.trixnityx.commands

import kotlinx.coroutines.flow.first
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.room
import net.folivo.trixnity.client.room.message.MessageBuilder
import net.folivo.trixnity.client.room.message.reply
import net.folivo.trixnity.client.store.Room
import net.folivo.trixnity.client.store.RoomUser
import net.folivo.trixnity.client.store.TimelineEvent
import net.folivo.trixnity.client.user

internal class TextMessageEventResponse(
	override val client: MatrixClient,
	override val event: TimelineEvent,
	override val lang: String,
	override val message: String
) : CommandAction {
	override val arguments: List<String> by lazy {
		message.removePrefix(PREFIX).split(" ").let {
			/*
			As long as the split list is not 1 item long (being the command),
			we can remove the command for only the arguments.
			Other wise an empty list
			 */
			if (it.size != 1)
				it.subList(1, it.size)
			else emptyList()
		}
	}

	/**
	 * Used to cache the response in [getAuthor] to increase performance.
	 */
	private var author: RoomUser? = null

	override suspend fun getAuthor(): RoomUser? =
		if (author != null) {
			author
		} else {
			client.user.getById(event.roomId, event.event.sender).first().also { author = it }
		}

	/**
	 * Used to cache the response in [getRoom] to increase performance.
	 */
	private var room: Room? = null

	override suspend fun getRoom(): Room? =
		if (room != null) {
			room
		} else {
			client.room.getById(event.roomId).first().also { room = it }
		}

	override suspend fun respond(
		asReply: Boolean,
		message: suspend MessageBuilder.() -> Unit
	) {
		client.room.sendMessage(event.roomId) {
			message()
			if (asReply)
				reply(event)
		}
	}
}