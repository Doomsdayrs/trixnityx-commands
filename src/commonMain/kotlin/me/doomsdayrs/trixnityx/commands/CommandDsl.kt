/*
 * Trixnityx-command : command extensions for trixnity
 * Copyright (C) 2022 Doomsdayrs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package me.doomsdayrs.trixnityx.commands

@DslMarker
public annotation class TrixnityCommand

/**
 * DSL for declaring a command
 */
@TrixnityCommand
public fun CommandDefineScope.command(build: CommandSpecification.() -> Unit) {
	commands.add(CommandSpecification().apply(build).toData())
}