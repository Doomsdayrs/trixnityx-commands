/*
 * Trixnityx-command : command extensions for trixnity
 * Copyright (C) 2022 Doomsdayrs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package me.doomsdayrs.trixnityx.commands

/**
 * Defines a command
 */
public interface Command {
	public val names: Map<String, String>
	public val descriptions: Map<String, String>
	public val action: suspend CommandAction.() -> Unit
}

/**
 * Data class version of [Command]
 */
public data class CommandData(
	override val names: Map<String, String>,
	override val descriptions: Map<String, String>,
	override val action: suspend CommandAction.() -> Unit
) : Command

/**
 * Builder class for command DSL
 */
public class CommandSpecification : Command {
	override val names: HashMap<String, String> = HashMap()
	override val descriptions: HashMap<String, String> = HashMap()
	override lateinit var action: suspend CommandAction.() -> Unit

	public fun action(action: suspend CommandAction.() -> Unit) {
		this.action = action
	}

	public fun toData(): CommandData = CommandData(names, descriptions, action)
}