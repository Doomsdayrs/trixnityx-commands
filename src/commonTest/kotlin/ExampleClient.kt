import io.ktor.http.Url
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import me.doomsdayrs.trixnityx.commands.CommandHandler
import me.doomsdayrs.trixnityx.commands.PREFIX
import me.doomsdayrs.trixnityx.commands.command
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.fromStore
import net.folivo.trixnity.client.login
import net.folivo.trixnity.client.media.InMemoryMediaStore
import net.folivo.trixnity.client.room
import net.folivo.trixnity.client.room.message.text
import net.folivo.trixnity.client.store.repository.createInMemoryRepositoriesModule
import net.folivo.trixnity.clientserverapi.model.authentication.IdentifierType
import kotlin.collections.set

class ExampleClient {
	fun main() {
		runBlocking {
			val scope = CoroutineScope(Dispatchers.Default)
			val commandScope = CoroutineScope(Dispatchers.IO)

			val mediaStore = InMemoryMediaStore()
			val repoModule = createInMemoryRepositoriesModule()
			PREFIX = "!"

			val matrixClient = MatrixClient.fromStore(
				repositoriesModule = repoModule,
				mediaStore = mediaStore,
				scope = scope
			).getOrThrow() ?: MatrixClient.login(
				baseUrl = Url("https://example.org"),
				identifier = IdentifierType.User("username"),
				password = "password",
				scope = scope,
				repositoriesModule = repoModule,
				mediaStore = mediaStore
			).getOrThrow()

			matrixClient.startSync()

			val conti = MutableStateFlow(true)

			val handler = CommandHandler(matrixClient)

			handler.register {
				command {
					names["eng"] = "hello"
					descriptions["eng"] = "Greet the bot"
					action {
						respond {
							text("Wow")
						}
					}
				}
			}

			// Run in the command scope
			val handlerJob = commandScope.launch {
				// Launch under supervisor scope to allow one a command to fail
				supervisorScope {
					matrixClient.room.getTimelineEventsFromNowOn().collect {
						// Launch command in a new thread to ensure fast responses
						launch {
							handler.feedEvent(it)
						}
					}
				}
			}

			// Wait for the end
			conti.first { !it }

			handlerJob.cancel()

			matrixClient.stopSync(true)
		}
	}
}